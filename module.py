import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import ticker
import pprint
from datetime import datetime as dt

# Beginning to 1929 = bull, 1929-1949 = bear, 1949-1969 = bull, 1969-1982 = bear etc.
secular_cutoff_dates = ["1929-10-01", "1949-06-01", "1969-05-01", "1982-08-02", "2000-03-01", "2011-09-01"]
first_secular_change = secular_cutoff_dates[0]
last_secular_change = secular_cutoff_dates[len(secular_cutoff_dates)-1]

snp_df = pd.read_excel('data.xlsx', "Sheet1")
snp_df["Date"]=pd.to_datetime(snp_df["Date"])
snp_df = snp_df.rename(columns={'PX_LAST':'Value'})
snp_df = snp_df.set_index("Date")

secular_bears_df = [snp_df[secular_cutoff_dates[0]:secular_cutoff_dates[1]],
                    snp_df[secular_cutoff_dates[2]:secular_cutoff_dates[3]],
                    snp_df[secular_cutoff_dates[4]:secular_cutoff_dates[5]]]
secular_bulls_df = [snp_df[secular_cutoff_dates[1]:secular_cutoff_dates[2]],
                    snp_df[secular_cutoff_dates[3]:secular_cutoff_dates[4]]]
current = snp_df[secular_cutoff_dates[5]:]


def compute_extrema(df, upper_threshold, lower_threshold):
    (ind_prior_high, prior_high) = (df.index[0], df.iloc[0,0]) 
    (ind_prior_low, prior_low) = (df.index[0], df.iloc[0,0]) 
    prior_minima = True
    maxima = []
    minima = []
    for ind in df.index:
        if prior_minima:
            if df["Value"][ind] < prior_low:
                ind_prior_low = ind
                prior_low = df["Value"][ind]
            elif df["Value"][ind] >= upper_threshold * prior_low:
                minima.append(ind_prior_low)
                prior_minima = False
                ind_prior_high = ind
                prior_high = df["Value"][ind]
        if not prior_minima:
            if df["Value"][ind] > prior_high:
                ind_prior_high = ind
                prior_high = df["Value"][ind]
            elif df["Value"][ind] <= lower_threshold * prior_high:
                maxima.append(ind_prior_high)
                prior_minima = True
                ind_prior_low = ind
                prior_low = df["Value"][ind]
    return (minima, maxima)


def count_cycle_days(snp_df, minima, maxima, cyclical_days_threshold = 0, beginning_secular_cycle = "bull"):
    local_df = snp_df.copy()
    local_df['secular'] = np.nan
    local_df['cyclical'] = np.nan
    local_df['cyclical'].iat[0] = beginning_secular_cycle
    local_df['secular'].iat[0] = beginning_secular_cycle
    cutoffs_as_dt = [dt.strptime(x, "%Y-%m-%d") for x in secular_cutoff_dates] 
    local_df.loc[local_df.index.intersection(cutoffs_as_dt[::2]), 'secular'] = 'bear'
    local_df.loc[local_df.index.intersection(cutoffs_as_dt[1::2]), 'secular'] = 'bull'
    local_df.loc[minima, 'cyclical'] = 'bull'
    local_df.loc[maxima, 'cyclical'] = 'bear'
    local_df = local_df.ffill()
    tmp = minima + maxima
    tmp.sort()
    print(pd.concat([local_df.iloc[0:1], local_df.loc[tmp], local_df.iloc[-2:-1]]))
    local_df['combined'] = local_df['secular'] + "-" + local_df['cyclical']
    # Defining zoomed -- dataframe to hold the time period between the first secular change and last secular change
    # in the data.
    zoomed = local_df[first_secular_change:last_secular_change].iloc[:-1] # Get rid of the first day after last 
                                                                          # secular change
    cyclical_df = zoomed.groupby(zoomed['combined'].ne(zoomed['combined'].shift()).cumsum())['combined'].value_counts()
    secular_df = zoomed.groupby(zoomed['secular'].ne(zoomed['secular'].shift()).cumsum())['secular'].value_counts()
    secular_bear_days_mean = secular_df[secular_df.index.isin(['bear'], level=1)].mean()
    secular_bull_days_mean = secular_df[secular_df.index.isin(['bull'], level=1)].mean()
    secular_bear_cyclical_bear_mean = cyclical_df[cyclical_df.index.isin(['bear-bear'], level=1) \
                                                  & (cyclical_df >= cyclical_days_threshold)].mean()
    secular_bear_cyclical_bull_mean = cyclical_df[cyclical_df.index.isin(['bear-bull'], level=1) \
                                                  & (cyclical_df >= cyclical_days_threshold)].mean()
    secular_bull_cyclical_bear_mean = cyclical_df[cyclical_df.index.isin(['bull-bear'], level=1) \
                                                  & (cyclical_df >= cyclical_days_threshold)].mean()
    secular_bull_cyclical_bull_mean = cyclical_df[cyclical_df.index.isin(['bull-bull'], level=1) \
                                                  & (cyclical_df >= cyclical_days_threshold)].mean()
    # Get lengths of current secular and cyclical cycle
    current_secular_cycle_days = len(local_df[secular_cutoff_dates[-1]:])
    current_cyclical_cycle_days = len(local_df[max(minima[-1], maxima[-1]):])
    current_cyclical_cycle_type = 'bear' if max(minima[-1], maxima[-1]) == maxima[-1] else 'bull'
    # Add code for current secular bull and bear averages
    print(cyclical_df)
    # print(avg_days[avg_days.index.isin(['bear-bull', 'bear-bear'], level=1)])
    # print(avg_days[avg_days.index.isin(['bear-bull', 'bear-bear'], level=1)].sum())
    # print(avg_days[avg_days.index.isin(['bull-bull', 'bull-bear'], level=1)].sum())
    print("Mean secular bear days: {}".format(secular_bear_days_mean))
    print("Mean secular bull days: {}".format(secular_bull_days_mean))
    print("Mean secular bear cyclical bear days: {}".format(secular_bear_cyclical_bear_mean))
    print("Mean secular bear cyclical bull days: {}".format(secular_bear_cyclical_bull_mean))
    print("Mean secular bull cyclical bear days: {}".format(secular_bull_cyclical_bear_mean))
    print("Mean secular bull cyclical bull days: {}".format(secular_bull_cyclical_bull_mean))
    
    return (secular_bear_days_mean, secular_bull_days_mean, secular_bear_cyclical_bear_mean,
            secular_bear_cyclical_bull_mean, secular_bull_cyclical_bear_mean, secular_bull_cyclical_bull_mean,
            current_secular_cycle_days, current_cyclical_cycle_days, current_cyclical_cycle_type)

